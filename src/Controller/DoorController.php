<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DoorController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {

        $result = $this->door('P......P......');
        dump($result);

        return $this->render('door/index.html.twig', [
            'controller_name' => 'DoorController',
        ]);
    }

    public function door($input): string
    {
        // 0 - not moving, 1 - moving
        $moving = 0;
        // 0 - up, 1 - down
        $direction = 0;
        $step = 0;
        $result = '';

        foreach (str_split($input) as $tick){
            if($tick == '.'){
                if($moving == 0){
                    $result .= $step;
                }else{
                    if($step == 5 || $step == 0){
                        $moving = 0;
                        if($direction == 0){
                            $direction = 1;
                        }
                        $result .= $step;
                    }else{
                        if($direction == 0)
                            $step++;
                        else
                            $step--;
                        $result .= $step;
                    }
                }
            }elseif($tick == 'P'){
                if($moving == 0){
                    if($direction == 0)
                        $step++;
                    else
                        $step--;
                    $moving = 1;
                    $result .= $step;
                }elseif($moving == 1){
                    $moving = 0;
                    $result .= $step;
                }
            }elseif($tick == 'O'){
                if($direction == 0){
                    $direction = 1;
                    $step--;
                }else{
                    $direction = 0;
                    $step++;
                }
                $result .= $step;
            }
        }
        return $result;
    }

}
