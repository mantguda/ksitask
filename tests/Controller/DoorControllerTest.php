<?php

namespace App\Tests\Controller;

use App\Controller\DoorController;
use PHPUnit\Framework\TestCase;

class DoorControllerTest extends TestCase
{
    public function testDoorController(): void
    {
        $door = new DoorController();

        $this->assertEquals('', $door->door(''));
        $this->assertEquals('12345' . str_repeat('5',10), $door->door('P....' . str_repeat('.',10)));
        $this->assertEquals('12345554321000', $door->door('P......P......'));
        $this->assertEquals('12222', $door->door('P.P..'));
        $this->assertEquals('122234555', $door->door('P.P.P....'));
        $this->assertEquals('1210000', $door->door('P.O....'));
        $this->assertEquals('1232222100', $door->door('P..OP..P..'));
        $this->assertEquals('001234321000', $door->door('..P...O.....'));
    }
}
